package com.boilerplateapp.modules;

import android.app.Activity;

import com.boilerplateapp.Navigator;
import com.boilerplateapp.scope.PerActivity;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;

@Module
@PerActivity
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    public Activity getActivity() { return this.activity; }

    @Provides
    public Navigator getTicketNavigator() {
        return  new Navigator(this.activity);
    }

    @Provides
    public OkHttpClient okHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    public Request.Builder requestBuilder() {
        return new Request.Builder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Origin", "https://flexmoney.in")
                .url("https://staging.flexmoney.in/app/dummy-card-details/submit");

    }

    @Provides
    public Gson gson() {
        return new Gson();
    }
}
