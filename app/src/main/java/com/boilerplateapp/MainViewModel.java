package com.boilerplateapp;

import android.app.Activity;
import android.app.Dialog;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.boilerplateapp.models.StoreCardRequestData;
import com.boilerplateapp.models.StoreCardResponseData;
import com.boilerplateapp.scope.PerActivity;
import com.boilerplateapp.usecase.StoreCardApiUseCase;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by rishabhjain on 8/4/18.
 */
@PerActivity
public class MainViewModel extends BaseObservable {

    private ObservableField<String> cardNo = new ObservableField<>();
    private ObservableField<String> expiryMonth = new ObservableField<>();
    private ObservableField<String> expiryYear = new ObservableField<>();
    private ObservableField<String> name = new ObservableField<>();
    private ObservableField<String> cvv = new ObservableField<>();

    private TextInputLayout tilCardNo, tilMonth, tilDate, tilName, tilCvv;

    @Inject
    public StoreCardApiUseCase storeCardApiUseCase;

    @Inject
    public Activity context;

    @Inject
    public Gson gson;

    @Inject
    public MainViewModel() {

    }

    @Bindable
    public ObservableField<String> getCardNo() {
        return cardNo;
    }

    @Bindable
    public ObservableField<String> getExpiryMonth() {
        return expiryMonth;
    }

    @Bindable
    public ObservableField<String> getExpiryYear() {
        return expiryYear;
    }

    @Bindable
    public ObservableField<String> getName() {
        return name;
    }

    @Bindable
    public ObservableField<String> getCvv() {
        return cvv;
    }

    public void onLoad(TextInputLayout tilCardNo, TextInputLayout tilMonth, TextInputLayout tilDate,
                       TextInputLayout tilName, TextInputLayout tilCvv) {
        this.tilCardNo = tilCardNo;
        this.tilMonth = tilMonth;
        this.tilDate = tilDate;
        this.tilName = tilName;
        this.tilCvv = tilCvv;
    }

    public void makeApiCall(View view) {

        if (checkForInvalidData()) {
            return;
        }

        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.show();

        final StoreCardRequestData requestData = new StoreCardRequestData(cardNo.get(), expiryMonth.get(),
                expiryYear.get(), name.get(), cvv.get());
        storeCardApiUseCase.execute(requestData).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<String>() {

                    @Override
                    public void onNext(String string) {

                        StoreCardResponseData responseData = gson.fromJson(string, StoreCardResponseData.class);
                        dialog.dismiss();
                        if (responseData.isSuccess()) {
                            Toast.makeText(context.getApplicationContext(), "Success.", Toast.LENGTH_LONG).show();
                        } else {
                            JsonParser parser = new JsonParser();
                            JsonElement tradeElement = parser.parse(string);
                            Toast.makeText(context.getApplicationContext(), ((JsonObject) tradeElement).get("data").toString(), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        Toast.makeText(context.getApplicationContext(), "Could not connect to the server.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public boolean checkForInvalidData() {
        boolean error = false;
        if (cardNo.get() == null || cardNo.get().length() < 10) {
            tilCardNo.setErrorEnabled(true);
            tilCardNo.setError("Incorrect card number");
            error = true;
        } else {
            tilCardNo.setError(null);
        }

        if (expiryMonth.get() == null || Integer.parseInt(expiryMonth.get()) > 12 || Integer.parseInt(expiryMonth.get()) < 1) {
            tilMonth.setErrorEnabled(true);
            tilMonth.setError("Incorrect month");
            error = true;
        } else {
            tilMonth.setError(null);
        }

        if (expiryYear.get() == null || expiryYear.get().length() < 2) {
            tilDate.setErrorEnabled(true);
            tilDate.setError("Incorrect year");
            error = true;
        } else {
            tilDate.setError(null);
        }

        if (name.get() == null || name.get().equals("")) {
            tilName.setErrorEnabled(true);
            tilName.setError("Incorrect name");
            error = true;
        } else {
            tilName.setError(null);
        }

        if (cvv.get() == null || cvv.get().length() < 3) {
            tilCvv.setErrorEnabled(true);
            tilCvv.setError("Incorrect cvv");
            error = true;
        } else {
            tilCvv.setError(null);
        }

        return error;
    }
}
