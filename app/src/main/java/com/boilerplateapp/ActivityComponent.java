package com.boilerplateapp;

import com.boilerplateapp.modules.ActivityModule;
import com.boilerplateapp.scope.PerActivity;

import dagger.Component;

/**
 * Created by rishabhjain on 8/4/18.
 */


@PerActivity
@Component(modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
