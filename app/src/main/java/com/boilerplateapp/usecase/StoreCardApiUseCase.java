package com.boilerplateapp.usecase;

import android.util.Log;

import com.boilerplateapp.models.StoreCardRequestData;
import com.boilerplateapp.scope.PerActivity;
import com.google.gson.Gson;


import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by rishabhjain on 8/4/18.
 */

@PerActivity
public class StoreCardApiUseCase {

    @Inject
    public OkHttpClient okHttpClient;

    @Inject
    public Request.Builder requestBuilder;

    @Inject
    public Gson gson;

    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json");


    @Inject
    public StoreCardApiUseCase() {}

    public Observable<String> execute(final StoreCardRequestData storeCardRequestData) {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {

                requestBuilder.post(RequestBody.create(MEDIA_TYPE, gson.toJson(storeCardRequestData)));
                Response response = okHttpClient.newCall(requestBuilder.build()).execute();
                String responseString = response.body().string();
                Log.e("response", responseString);

                if (response.code() == 200) {
                    e.onNext(responseString);
                } else {
                    e.onError(new RuntimeException("Some error occurred."));
                }
            }
        });
    }
}
