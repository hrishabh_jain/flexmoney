package com.boilerplateapp;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.boilerplateapp.databinding.ActivityMainBinding;
import com.boilerplateapp.modules.ActivityModule;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    @Inject
    public MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder().activityModule(new ActivityModule(this)).build().inject(this);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setVariable(BR.model, mainViewModel);

        mainViewModel.onLoad(binding.tilCardNo, binding.tilMonth, binding.tilYear, binding.tilName, binding.tilCvv);
    }
}
