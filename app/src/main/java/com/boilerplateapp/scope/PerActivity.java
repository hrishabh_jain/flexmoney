package com.boilerplateapp.scope;

import javax.inject.Scope;

/**
 * Created by Rishabh on 17/06/17.
 */

@Scope
public @interface PerActivity {
}
