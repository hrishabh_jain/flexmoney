package com.boilerplateapp.models;

/**
 * Created by rishabhjain on 8/4/18.
 */

public class StoreCardRequestData {
    private String cardNo;
    private String expiryMonth;
    private String expiryYear;
    private String name;
    private String cvv;

    public StoreCardRequestData(String cardNo, String expiryMonth, String expiryYear, String name, String cvv) {
        this.cardNo = cardNo;
        this.expiryMonth = expiryMonth;
        this.expiryYear = expiryYear;
        this.name = name;
        this.cvv = cvv;
    }
}
